<?php

return (new MomentumStudio\CodingStandards\LibraryConfigBuilder)
    ->project('Composer Presets')
    ->openSourceCopyright('MIT', 'https://opensource.org/licenses/MIT', 'https://gitlab.com/momentumstudio/composer-presets')
    ->finder(
        PhpCsFixer\Finder::create()
            ->in(__DIR__ . '/app')
            ->in(__DIR__ . '/config')
            ->in(__DIR__ . '/tests')
    )
    ->requirePhp('7.2.0')
    ->build();
