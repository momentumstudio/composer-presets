<p align="center">
    <img src="https://gitlab.com/momentumstudio/composer-presets/raw/master/logo.png" alt="Logo Image">
</p>

# Composer Presets

This project is licensed [MIT](https://opensource.org/licenses/MIT).

Composer Presets by [Momentum Studio](https://momentum.studio) is a tool that allows you
to store one or more presets for bootstrapping a project. There are a set a default presets provided
or you can simply edit an provide your own, in JSON format.

## Installation & Usage

**WARNING: This is in a very early alpha state, so may error a lot!**

> Requires

* PHP 7.2.0+
* [Laravel](https://laravel.com) 6.0+

Use Composer to install Composer Presets as a global dependency on your machine:

`composer global require momentumstudio/composer-presets:dev-master`

Ensure Composer is also in your `$PATH`.

You can then run a preset by running `preset`. This will show a menu with the current
default presets.

Running `preset install {name} {directory}` will let you run a preset on a given directory.
If the directory doesn't exist, it will be created automatically.

To create your own presets, run `preset edit {name}`. This will open your default editor.
If you are using the same name as a default preset, the editor will copy its config ready
for you to modify to your needs. This allows you to create your own default preset for Laravel!

# Contributing

Please see [CONTRIBUTING.md](https://gitlab.com/momentumstudio/composer-presets/blob/master/CONTRIBUTING.md) for details
about how you can help!
