<?php

/**
 * (c) Copyright Momentum Studio Ltd. All Rights Reserved.
 * This code is a part of Composer Presets (an open source project) under the MIT license.
 * You must adhere to the licensing restrictions found at https://opensource.org/licenses/MIT
 * For support, please visit https://gitlab.com/momentumstudio/composer-presets
 */

declare(strict_types=1);

namespace Tests;

use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     */
    public function createApplication(): \Illuminate\Foundation\Application
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }
}
