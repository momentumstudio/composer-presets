### Description of Feature Request

(Describe the details of your awesome new feature idea)

### Potential implementation

(Detail, as if you were implementing it, how you would go about implementing this feature)

### Release

(Tell us which version we should release it in and why)


/relabel ~Feature ~"Needs Triage"
/due in 3 days
/subscribe
