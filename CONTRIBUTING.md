## Creating Issues

We use GitLab Issues as the only place to track all issues with the project. Please create an issue
using a pre-existing template. This will make it a lot easier for us to gather information about
your specific installation.

Please follow GitLab's own code of conduct, of which the standards are as follows:

> Examples of behavior that contributes to creating a positive environment include:
> 
> 1. Using welcoming and inclusive language.
> 1. Being respectful of differing viewpoints and experiences.
> 1. Gracefully accepting constructive criticism.
> 1. Focusing on what is best for the community.
> 1. Showing empathy towards other community members.
> 
> Examples of unacceptable behavior by participants include:
>   
> 1. The use of sexualized language or imagery and unwelcome sexual attention or advances.
> 1. Trolling, insulting/derogatory comments, and personal or political attacks.
> 1. Public or private harassment.
> 1. Publishing others' private information, such as a physical or electronic address, without explicit permission.
> 1. Other conduct which could reasonably be considered inappropriate in a professional setting.

## Contributing Code

If you wish to contribute some code to the project, you must also follow the same rules above when
creating your MR.

We use GitLab CI to automatically scan each MR that comes in, these checks include:

1. Code style check, following Momentum Studio's standards.
2. Security check using Sensio Lab's database.
3. PHPStan code analysis
4. PHPUnit testing in PHP 7.1, 7.2 and 7.3.
5. PHPUnit code coverage check.
6. Composer require scan, to ensure all dependencies that are required are actually being used.

You can run these checks manually before submitting your MR by running `composer install` and then
running:

1. `composer cs` - This automatically reformats your code to meet our standard.
2. `vendor/bin/phpstan analyse` - This will tell PHPStan to run analysis against the codebase.
3. `vendor/bin/phpunit` - This will run all PHPUnit tests.

When submitting your MR, we are expecting it to pass these CI checks. We require at least one approved
review from a member of Momentum Studio before code will be merged. We reserve the right to close MRs if we
do not accept the code change and we also reserve the right to modify your MR and code. Any code
submitted to us is the sole copyright of Momentum Studio and is covered under the MIT license.

If you do not wish to contribute, you are free to create a fork or modify our code locally for your own purposes.

### Code Coverage

Usually, we would be strict about testing coverage. However, since this is a local developer-only style
project, we are not looking for strict code coverage.

## Golden Rule

If you're unsure, don't be afraid to ask! We're all here to improve and maintain this project and therefore
all questions are welcome. For example, if you're new to making MRs then don't panic! We can guide you through
submitting the MR and we can clean it up and ensure its good quality ourselves.
