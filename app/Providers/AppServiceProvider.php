<?php

/**
 * (c) Copyright Momentum Studio Ltd. All Rights Reserved.
 * This code is a part of Composer Presets (an open source project) under the MIT license.
 * You must adhere to the licensing restrictions found at https://opensource.org/licenses/MIT
 * For support, please visit https://gitlab.com/momentumstudio/composer-presets
 */

declare(strict_types=1);

namespace MomentumStudio\ComposerPresets\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
    }
}
