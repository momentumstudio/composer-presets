<?php

/**
 * (c) Copyright Momentum Studio Ltd. All Rights Reserved.
 * This code is a part of Composer Presets (an open source project) under the MIT license.
 * You must adhere to the licensing restrictions found at https://opensource.org/licenses/MIT
 * For support, please visit https://gitlab.com/momentumstudio/composer-presets
 */

declare(strict_types=1);

namespace MomentumStudio\ComposerPresets;

/**
 * Represents a single preset, enumerating the configuration from the raw JSON.
 */
class Preset
{
    /** @var string Name of preset. */
    private $name;

    /** @var array<mixed, mixed> Config loaded from JSON file. */
    private $config;

    /** @var bool Is this file one created by the user, outside of this project? */
    private $isCustom;

    public function __construct(string $name, string $config, bool $isCustom)
    {
        $this->name = $name;
        $this->config = json_decode($config, true);
        $this->isCustom = $isCustom;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isCustom(): bool
    {
        return $this->isCustom;
    }

    public function getBase(): ?string
    {
        $base = $this->config['base'];

        if (!is_string($base) || false == $base || '' === $base) {
            return null;
        }

        return $base;
    }

    /**
     * @return array<mixed, mixed>
     */
    public function getRequires(): array
    {
        $requires = $this->config['require'] ?? [];

        if (!is_array($requires) || false == $requires || '' == $requires) {
            return [];
        }

        return $requires;
    }

    /**
     * @return array<mixed, mixed>
     */
    public function getDevRequires(): array
    {
        $requires = $this->config['require-dev'] ?? [];

        if (!is_array($requires) || false == $requires || '' == $requires) {
            return [];
        }

        return $requires;
    }

    /**
     * @return array<mixed, mixed>
     */
    public function getCommands(): array
    {
        $commands = $this->config['commands'] ?? [];

        if (!is_array($commands) || false == $commands || '' == $commands) {
            return [];
        }

        return $commands;
    }

    /**
     * @return array<mixed, mixed>
     */
    public function getRepositories(): array
    {
        $repositories = $this->config['repositories'] ?? [];

        if (!is_array($repositories) || false == $repositories || '' == $repositories) {
            return [];
        }

        return $repositories;
    }

    /**
     * @return array<mixed, mixed>
     */
    public function getConfig(): array
    {
        $config = $this->config['config'] ?? [];

        if (!is_array($config) || false == $config || '' == $config) {
            return [];
        }

        return $config;
    }

    public function __toString(): string
    {
        $string = $this->name;

        if ($this->isCustom) {
            $string .= ' *';
        }

        return $string;
    }
}
