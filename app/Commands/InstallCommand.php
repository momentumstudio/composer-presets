<?php

/**
 * (c) Copyright Momentum Studio Ltd. All Rights Reserved.
 * This code is a part of Composer Presets (an open source project) under the MIT license.
 * You must adhere to the licensing restrictions found at https://opensource.org/licenses/MIT
 * For support, please visit https://gitlab.com/momentumstudio/composer-presets
 */

declare(strict_types=1);

namespace MomentumStudio\ComposerPresets\Commands;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;
use MomentumStudio\ComposerPresets\Preset;
use MomentumStudio\ComposerPresets\PresetManager;
use NunoMaduro\LaravelConsoleMenu\Menu;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * @method Menu menu(string $title, array $options)
 */
class InstallCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'install
        {name? : Name of the preset to install, leave blank to see a menu}
        {directory? : Which directory to install in, leave blank to use current directory}
    ';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Install a preset to the given directory.';

    /**
     * Execute the console command.
     *
     * @return int Exit code
     */
    public function handle(Filesystem $filesystem, PresetManager $presetManager): int
    {
        $name = $this->argument('name');
        $directory = $this->argument('directory') ?? getcwd();

        if (!is_string($name)) {
            $name = null;
        }
        if (!is_string($directory)) {
            $directory = null;
        }

        if (null === $name || '' === $name || Str::contains($name, DIRECTORY_SEPARATOR)) {
            // Show a menu if the name isn't there or looks like a directory
            if (null !== $name && Str::contains($name, DIRECTORY_SEPARATOR)) {
                $directory = $name;
                $name = null;
            }

            // Find all the presets and display them in a nice menu, alphabetically.
            $presets = $presetManager->findAll();
            $name = $this->menu(
                'Preset list',
                $presets->map(function (Preset $preset): string {
                    return $preset->getName();
                })->toArray()
            )
                ->disableDefaultItems()
                ->open();
        }

        // Sanity checks
        if (null === $name || '' === $name) {
            $this->error('No preset name given.');

            return 1;
        }
        if (null === $directory || '' === $directory) {
            $directory = getcwd();

            if (false === $directory) {
                $this->error('Cannot get current working directory, please explicitly set a directory.');

                return 1;
            }
        }

        // Normalise $directory
        $directory = realpath($directory);

        // Install preset to directory
        $preset = $presetManager->find($name);
        if (null === $preset) {
            $this->error("Preset ${name} not found. To create it, run `preset edit ${name}`.");

            return 1;
        }

        $base = $preset->getBase();
        if (null !== $base && '' !== $base) {
            // Split the options
            $options = [];
            $package = null;
            foreach (explode(' ', $base) as $item) {
                if (Str::contains($item, '/')) {
                    $package = $item;
                } else {
                    $options[] = $item;
                }
            }

            if (null === $package || '' === $package) {
                $this->error('Could not work out base package.');

                return 0;
            }

            // Call composer create-project
            $options = implode(' ', $options);
            $this->task("Installing ${package}", function () use ($package, $directory, $options): bool {
                return $this->runCliCommand([
                    'composer',
                    'create-project',
                    $package,
                    $directory,
                    $options,
                ]);
            });
        } else {
            // Create the project folder, blank.
            if (false === $directory || (!$filesystem->isDirectory($directory) && !mkdir($directory))) {
                $this->error("Could not create ${directory}");

                return 1;
            }
        }
        assert($directory !== false);

        // Add any repositories
        $repositories = $preset->getRepositories();
        if (count($repositories) > 0) {
            $this->task('Adding custom repositories', function () use ($filesystem, $directory, $repositories): bool {
                return $this->mergeJsonConfig($filesystem, 'repositories', $directory, $repositories);
            });
        }

        // Add any config
        $config = $preset->getConfig();
        if (count($config) > 0) {
            $this->task('Adding custom config', function () use ($filesystem, $directory, $config): bool {
                return $this->mergeJsonConfig($filesystem, 'config', $directory, $config);
            });
        }

        // Install additional composer packages
        $isDev = true;
        foreach ([$preset->getDevRequires(), $preset->getRequires()] as $packages) {
            foreach ($packages as $package => $version) {
                $this->task("Installing `${package}` (${version})", function () use ($package, $version, $isDev): bool {
                    return $this->runCliCommand([
                        'composer',
                        'require',
                        $package . ($version !== 'latest' ? ':' . $version : ''),
                        '--no-interaction',
                        '--no-suggest',
                        '--no-progress',
                        $isDev ? '--dev' : '',
                    ]);
                });
            }

            $isDev = false;
        }

        // Run extra commands
        foreach ($preset->getCommands() as $command) {
            $this->task("Running `${command}`", function () use ($command): bool {
                return $this->runCliCommand([$command]);
            });
        }

        $this->info('Done.');

        return 0;
    }

    /**
     * @param array<string|false> $command
     */
    private function runCliCommand(array $command, ?string $cwd = null): bool
    {
        $process = new Process($command, $cwd, null, null, null);
        $exit = $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $exit === 0;
    }

    /**
     * @param array<mixed, mixed> $value
     */
    private function mergeJsonConfig(Filesystem $filesystem, string $key, string $directory, array $value): bool
    {
        try {
            $file = $directory . DIRECTORY_SEPARATOR . 'composer.json';
            $currentJson = $filesystem->get($file);
            $currentJson = json_decode($currentJson, true);

            if (!isset($currentJson[$key])) {
                $currentJson[$key] = $value;
            } else {
                $currentJson[$key] = array_merge($currentJson[$key], $value);
            }

            $output = json_encode($currentJson, JSON_PRETTY_PRINT);
            if (false === $output) {
                return false;
            }

            return $filesystem->put($file, $output) !== false;
        } catch (FileNotFoundException $e) {
            return false;
        }
    }
}
