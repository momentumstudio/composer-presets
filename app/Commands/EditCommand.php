<?php

/**
 * (c) Copyright Momentum Studio Ltd. All Rights Reserved.
 * This code is a part of Composer Presets (an open source project) under the MIT license.
 * You must adhere to the licensing restrictions found at https://opensource.org/licenses/MIT
 * For support, please visit https://gitlab.com/momentumstudio/composer-presets
 */

declare(strict_types=1);

namespace MomentumStudio\ComposerPresets\Commands;

use Illuminate\Filesystem\Filesystem;
use LaravelZero\Framework\Commands\Command;
use MomentumStudio\ComposerPresets\PresetManager;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Create Edit / Override a preset.
 */
class EditCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'edit {name : Name of preset to edit}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Create / Edit / Override a preset, making your own custom version.';

    /**
     * Execute the console command.
     */
    public function handle(Filesystem $filesystem, PresetManager $presetManager): int
    {
        $name = $this->argument('name');
        if (!is_string($name)) {
            $name = null;
        }

        $filepath = $presetManager->getCustomRoot() . DIRECTORY_SEPARATOR . $name . '.json';
        $defaultFilepath = $presetManager->getDefaultRoot() . DIRECTORY_SEPARATOR . $name . '.json';

        if ($filesystem->isFile($filepath)) {
            if (!$this->launchEditor($filepath)) {
                $this->error("Could not launch an editor, we suggest you manually edit the file at `${filepath}`");

                return 1;
            }
        } else {
            // Copy file from default if it exists
            if ($filesystem->isFile($defaultFilepath)) {
                $filesystem->copy($defaultFilepath, $filepath);
            }

            if (!$this->launchEditor($filepath)) {
                $this->error("Could not launch an editor, we suggest you manually edit the file at `${filepath}`");

                return 1;
            }
        }

        return 0;
    }

    private function launchEditor(string $filepath): bool
    {
        $editor = env('EDITOR', null);
        if (!is_string($editor) || '' === $editor) {
            return false;
        }

        $process = new Process([
            $editor,
            $filepath,
        ]);

        try {
            $process->setTty(true);
            $process->mustRun(function ($type, $buffer): void {
                echo $buffer;
            });
        } catch (ProcessFailedException $e) {
            return false;
        }

        return true;
    }
}
