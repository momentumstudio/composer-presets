<?php

/**
 * (c) Copyright Momentum Studio Ltd. All Rights Reserved.
 * This code is a part of Composer Presets (an open source project) under the MIT license.
 * You must adhere to the licensing restrictions found at https://opensource.org/licenses/MIT
 * For support, please visit https://gitlab.com/momentumstudio/composer-presets
 */

declare(strict_types=1);

namespace MomentumStudio\ComposerPresets;

use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Finder\SplFileInfo;

class PresetManager
{
    /** @var Filesystem */
    private $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function find(string $name): ?Preset
    {
        return $this->findAll()->filter(function (Preset $preset) use ($name): bool {
            return $preset->getName() === $name;
        })->first();
    }

    /**
     * Get all available presets, in alphabetical order.
     *
     * @return PresetCollection<Preset>
     */
    public function findAll(): PresetCollection
    {
        $defaultPresets = $this->loadPresetsFromDirectory($this->getDefaultRoot(), false);
        $customPresets = $this->loadPresetsFromDirectory($this->getCustomRoot(), true);

        return $defaultPresets->merge($customPresets)->sort(function (Preset $a, Preset $b): int {
            if ($a->getName() === $b->getName()) {
                return 0;
            }

            return strcmp($a->getName(), $b->getName());
        });
    }

    /**
     * Get the root directory where the custom presets are stored.
     */
    public function getCustomRoot(): string
    {
        $composerHome = $this->getComposerHome();

        return $composerHome . DIRECTORY_SEPARATOR . 'presets';
    }

    /**
     * Get the root directory where the default presets are stored.
     */
    public function getDefaultRoot(): string
    {
        $composerHome = $this->getComposerHome();

        return implode(DIRECTORY_SEPARATOR, [
            $composerHome,
            'vendor',
            'momentumstudio',
            'composer-presets',
            'presets',
        ]);
    }

    private function getComposerHome(): string
    {
        $composerHome = env('COMPOSER_HOME');

        if (null === $composerHome) {
            // Composer must be using $XDG_CONFIG_HOME/composer
            $composerHome = env('XDG_CONFIG_HOME');

            if (null === $composerHome) {
                // Let's use a safer default
                $composerHome = env('HOME') . '/.config';
            }

            $composerHome .= '/composer';
        }

        return $composerHome;
    }

    /**
     * @return PresetCollection<Preset>
     */
    private function loadPresetsFromDirectory(string $directory, bool $isCustom): PresetCollection
    {
        if (!$this->filesystem->isDirectory($directory)) {
            return new PresetCollection();
        }

        $items = array_map(function (SplFileInfo $file) use ($isCustom) {
            if (
                $file->isReadable() &&
                strtolower($file->getExtension()) === 'json'
            ) {
                $filename = substr($file->getFilename(), 0, -5);

                return new Preset($filename, $file->getContents(), $isCustom);
            }

            return false;
        }, $this->filesystem->files($directory));

        return new PresetCollection($items);
    }
}
